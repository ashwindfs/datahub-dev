/*
var urlTest = "";
var client = new XMLHttpRequest();
client.open('GET', 'url.txt');
client.onreadystatechange = function() {
  alert(client.responseText);
	urlTest = client.responseText;
}
client.send();
console.log(urlTest);
*/
var dhApp = angular.module('dhApp', ['md.data.table', 'ngMaterial', 'ngRoute', 'ngMessages']);

dhApp.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.useXDomain = true;
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

dhApp.config (function ($routeProvider){
	
	$routeProvider
		.when('/', {
		templateUrl: '/landing.html',
		controller: 'mainCtrl'
	})
		.when('/search', {
		templateUrl: '/search.html',
		controller: 'mainCtrl'
	})
		.when('/filter', {
		templateUrl: '/filter.html',
		controller: 'mainCtrl'
	})
		.when('/salesheader/:receiptNumber', {
			templateUrl: '/salesDetails.html',
			controller: 'salesHeaderCtrl'
	})
		.when('/product/:sku', {
			templateUrl: '/product.html',
			controller: 'productCtrl'
	});
	
});

dhApp.config(function($mdThemingProvider) {
  $mdThemingProvider
    .theme('default')
    .primaryPalette('red')
    .accentPalette('blue-grey')
    .warnPalette('amber');
});