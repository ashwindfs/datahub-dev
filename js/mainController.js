var configData = JSON.parse(config);
var configslshdr = configData.slshdr;
var configslsline = configData.slsline;
var configproduct = configData.product;

var urlSlshdr = configslshdr;
var urlSlsline = configslsline;
var urlProd = configproduct;
console.log("urlSlshdr: " + urlSlshdr);
console.log("urlSlsline: " + urlSlsline);
console.log("urlProd: " + urlProd);

/* ng-controller for navigation bar */
dhApp.controller('navBarCtrl', function($scope, $location){
		$scope.tabSearch = function(){
			$location.path("/");
		};
		$scope.tabFilter = function(){
			$location.path("/filter");
		};
});

/* ng-controller for conduct search */
dhApp.controller('mainCtrl', ["$scope", "$routeParams", "$http", "$mdSidenav", function($scope, $routeParams, $http, $mdSidenav){
	
	$scope.clickFilterMN = function(val){
		
		urlSlshdr += "?$filter=MembershipNumber1 eq '" + val + "'";
		$http.get(urlSlshdr).then( function(response) {
					$scope.dataOutput = response.data.value;
					console.log("response: " + $scope.dataOutput);
					console.log("size: " + $scope.dataOutput.length); 
					if ($scope.dataOutput.length == 0){
						console.log("No data found");
						window.alert("No data found");
					}
		});
		urlSlshdr = configslshdr;
		//document.getElementById("txtMembershipNumber").value = "";
		$scope.memberNumberInput = '';
		$scope.transactionDateInput = '';
		
		$mdSidenav('searchTip').close();
	};
	
	$scope.clickFilterTD = function(val){
		urlSlshdr += "?$filter=TransactionDate eq '" + val + "'&$top=20";
		$http.get(urlSlshdr).then( function(response) {
					$scope.dataOutput = response.data.value;
					console.log("response: " + $scope.dataOutput);
					console.log("size: " + $scope.dataOutput.length); 
					if ($scope.dataOutput.length == 0){
						console.log("No data found");
						window.alert("No data found");
					}
		});
		urlSlshdr = configslshdr;
		document.getElementById("txtTransactionDate").value = "";
		$scope.memberNumberInput = '';
		$scope.transactionDateInput = '';
		
		$mdSidenav('searchTip').close();
	};
	
	/* //replaced with md-autocomplete
	$scope.slshdrMetadatas = slshdrMetadataArray;
	console.log($scope.slshdrMetadatas);
	*/
	$scope.advancedFilterStr = "";
	
	$scope.clickAdvancedFilter = function(selectedMetaData, selectedFilterOption, filterValue, selectedTopNumber){
		$mdSidenav('filterTip').close();
		
		urlSlshdr = configslshdr;
		
		if(selectedTopNumber === undefined){
			selectedTopNumber = 10;
		}
		
		if(selectedFilterOption === undefined || filterValue === undefined){
			window.alert("Please enter all fields before submitting.");
		} else{
			if ( selectedMetaData === "ReceiptNumber"){
				if( selectedFilterOption === "eq"){
					urlSlshdr += "?$filter=" + selectedMetaData + " " + selectedFilterOption + " " + filterValue;
					console.log(urlSlshdr);
				} else{
					urlSlshdr += "?$filter=" + selectedMetaData + " " + selectedFilterOption + " " + filterValue + "&$top=" + selectedTopNumber;
					console.log(urlSlshdr);
				}
			} else{
				urlSlshdr += "?$filter=" + selectedMetaData + " " + selectedFilterOption + " '" + filterValue + "'&$top=" + selectedTopNumber;
				console.log(urlSlshdr);
			}
			
			$http.get(urlSlshdr).then( function(response) {
				$scope.dataOutput = response.data.value;
				console.log("response: " + $scope.dataOutput);
				console.log("size: " + $scope.dataOutput.length); 
				if ($scope.dataOutput.length == 0){
					console.log("No data found");
					window.alert("No data found");
				}
			});
			
			$scope.selectedFilterOption = '';
			$scope.filterValue = '';
			$scope.selectedTopNumber = '';

			urlSlshdr = configslshdr;
		}
	};
	
	$scope.sortField = 'TransactionDate', 'TransactionTime';
	$scope.reverse = false;
	
}]);

/* ng-controller for loading sales details */
dhApp.controller('salesHeaderCtrl', ["$scope", "$routeParams", "$http", "$mdToast", function($scope, $routeParams, $http, $mdToast){
	var receiptNum = $routeParams.receiptNumber;
	$scope.rnum = receiptNum;
	urlSlshdr = configslshdr;
	urlSlshdr += "?$filter=ReceiptNumber eq " + receiptNum;
	
	$http.get(urlSlshdr).then( function(response) {
		$scope.dataOutputSalesHeader = response.data.value;
		console.log("response: " + $scope.dataOutputSalesHeader);
		console.log("size: " + $scope.dataOutputSalesHeader.length); 
		if ($scope.dataOutputSalesHeader.length == 0){
			console.log("No data found");
		}
	});
	
	//HTTP GET from slsline using ReceiptNumber
	urlSlsline += "?$filter=ReceiptNumber eq '" + receiptNum + "'";
	
	$http({
		method: 'GET',
		url: urlSlsline
	}).then(function successCallback(response) {
			// this callback will be called asynchronously
			// when the response is available
			$scope.dataOutputSalesLine = "";
			var outputSize = response.data.value.length;

			var outputRows = [];

			for(i=0; i < outputSize; i++){
				var lineNumberStr = response.data.value[i].LineNumber;
				var skuNumberStr = response.data.value[i].SkuNumber;
				var lineQuantityStr = response.data.value[i].LineQuantity; 
				var lineRetailStr = response.data.value[i].LineRetail;
				var lineTaxStr = response.data.value[i].LineTax;
				var receiptNumberStr = response.data.value[i].ReceiptNumber;

			 	var row = {
					"LineNumber": lineNumberStr,
					"SkuNumber": skuNumberStr,
					"LineQuantity": lineQuantityStr,
					"LineRetail": lineRetailStr,
					"LineTax": lineTaxStr
				}
			 	outputRows.push(row);
			}
		
			$scope.dataOutputSalesLineProduct = outputRows;
		
			$scope.clickItemQuickView = function(skuNum){
				var urlProdQuery = urlProd + skuNum;
				var prdCollectionDescription = "";
				var itdItemDescription = "";

				//HTTP GET from PRODUCT REST API
				$http.get(urlProdQuery)
					.then(function(responseProd){

						prdCollectionDescription = responseProd.data["prd:Product"]["0"]["prd:ProductDCS"]["prd:CollectionDescription"];
						console.log(prdCollectionDescription);
						itdItemDescription = responseProd.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][0]["itd:ItemDescription"];
						console.log(itdItemDescription);
					
						$scope.showItemInfo = function() {
							var message = itdItemDescription + " [" + prdCollectionDescription + "]";
							
							$mdToast.show($mdToast.simple({
								hideDelay: 6000,
								position: 'bottom right',
								content: message
							}));
						};

						$scope.showItemInfo();	
				});
				
			}
			

		}, function errorCallback(response) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			console.log("error");
		});
	urlSlshdr = configslshdr;
	urlSlsline = configslsline;
}]);

/** Data retrieved from HBASE ENRICHED_PRODUCT REST API (base64) **/
/*
dhApp.controller('productCtrl', ["$scope", "$routeParams", "$http", function($scope, $routeParams, $http){
	var sku = $routeParams.sku;
	$scope.sku = sku;
	
	/*
	var urlProd = root + "/ENRICHED_PRODUCT/" + sku + "/cf:Items.Items.Item.1.itd:ItemDescription.1.itd:ItemDescription,cf:Items.Items.Item.1.itd:ItemDescription.1.itd:ItemMarketingDescription,cf:Items.Items.Item.1.itd:ItemDescription.1.itd:ItemMarketingTitle";
	var prdtMetaDataArray = [];
	
	$http.get(urlProd).then( function(response) {
		console.log(response);
		var obj = response.data.Row["0"].Cell;
		console.log(obj);
		var sizeOfProperty = obj.length;
		console.log(sizeOfProperty);
		for (var i=0; i<sizeOfProperty; i++){
			var metadataVal = atob(obj[i].column);
			var dataVal = atob(obj[i].$);
			var colname = metadataVal.substring(metadataVal.lastIndexOf(":") + 1);
			//dataVal = dataVal.replace(/\n/g,"<br>");
			prdtMetaDataArray.push({ 
				column:metadataVal,
				columnName: colname,
				text:dataVal
			});
		};
	});
	
	$scope.dataOutputProduct = prdtMetaDataArray;
}]);
*/

/* ng-controller for loading product details */
/** using AngularJS for HTTP GET **/
dhApp.controller('productCtrl', ["$scope", "$routeParams", "$http", function($scope, $routeParams, $http){
		var sku = $routeParams.sku;
		$scope.sku = sku;
	 	
		var urlProdQuery = urlProd + sku;
		console.log(urlProdQuery);
		
		$http.get(urlProdQuery).then(function(response){
			console.log(response.data);

			$scope.itdItemMarketingTitleEn = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][0]["itd:ItemMarketingTitle"];
			$scope.itdItemMarketingDescriptionEn = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][0]["itd:ItemMarketingDescription"];
			$scope.itdItemDescription = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][0]["itd:ItemDescription"];

			$scope.itdItemMarketingTitleZh = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][1]["itd:ItemMarketingTitle"];
			$scope.itdItemMarketingDescriptionZh = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][1]["itd:ItemMarketingDescription"];

			$scope.itdItemMarketingTitleJa = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][8]["itd:ItemMarketingTitle"];
			$scope.itdItemMarketingDescriptionJa = response.data["prd:Product"][0]["prd:Items"]["prd:Item"][0]["itd:ItemDescription"][8]["itd:ItemMarketingDescription"];

			$scope.prdCollectionDescription = response.data["prd:Product"]["0"]["prd:ProductDCS"]["prd:CollectionDescription"];
			urlProdQuery = urlProd;
		});
}]);

/* ng-controller for loading side nav help tip */
dhApp.controller('sideNavCtrl', function ($scope, $timeout, $mdSidenav) {
	//$scope.toggleLeft = buildToggler('left');
	$scope.toggleSearchTip = buildToggler('searchTip');
	$scope.toggleFilterTip = buildToggler('filterTip');
	
	function buildToggler(componentId) {
		return function() {
			$mdSidenav(componentId).toggle();
			if(componentId == "searchTip"){
				$mdSidenav('filterTip').close();
			}
			if(componentId == "filterTip"){
				$mdSidenav('searchTip').close();
			}
		}
	}
});

function goBack() {
    window.history.back();
}