var configmetadata = configData.metadata;
var urlAutocompleteController = configmetadata;
console.log("urlMetadata: " + urlAutocompleteController);

dhApp.controller('autoCompleteController', autoCompleteController);
				
//Retrieving of metadata (XML)
var settings = {
	"async": true,
	"crossDomain": true,
	"url": urlAutocompleteController,
	"method": "GET"
}

var slshdrMetadataProperties = [];
//var entityTypeName = "";

$.ajax(settings).done(function (response) {
	//console.log(response);
	var obj = xmlToJson(response);
	//entityTypeName = obj["edmx:Edmx"]["edmx:DataServices"].Schema.EntityContainer.EntitySet["0"]["@attributes"].Name;  console.log("here: " + entityTypeName);
	var sizeOfProperty = obj["edmx:Edmx"]["edmx:DataServices"].Schema.EntityType["0"].Property.length;
	for (var i=0; i<sizeOfProperty; i++){
		var md = obj["edmx:Edmx"]["edmx:DataServices"].Schema.EntityType["0"].Property[i]["@attributes"].Name;
		//console.log(md);
		slshdrMetadataProperties.push({
			value: md.toLowerCase(),
			display: md
		});
	};
});

function autoCompleteController ($timeout, $q, $log) {
	 var self = this;
	 self.simulateQuery = true;
	 self.isDisabled    = false;
	 // list of states to be displayed
	 self.states        = loadProperties();
	 self.querySearch   = querySearch;
	 self.selectedItemChange = selectedItemChange;
	 self.searchTextChange   = searchTextChange;
	 self.newState = newState;
	 //self.etName = entityTypeName;
	 function newState(state) {
			alert("This functionality is yet to be implemented!");
	 }    
	 function querySearch (query) {
			var results = query ? self.states.filter( createFilterFor(query) ) : self.states, deferred;
			if (self.simulateQuery) {
				 deferred = $q.defer();
				 $timeout(function () { 
							 deferred.resolve( results ); 
						}, 
				Math.random() * 0, false);
				 return deferred.promise;
			} else {
				 return results;
			}
	 }
	 function searchTextChange(text) {
			$log.info('Text changed to ' + text);
	 }
	 function selectedItemChange(item) {
			$log.info('Item changed to ' + JSON.stringify(item));
	 }


	 //build list of states as map of key-value pairs
	 function loadProperties() {
		 return slshdrMetadataProperties;
	 }
	 //filter function for search query
	 function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(state) {
				 return (state.value.indexOf(lowercaseQuery) === 0);
			};
	 }
}  