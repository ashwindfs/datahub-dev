# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for?

### Quick summary

This application provides a Graphic User Interface for the web services provided by the Data Hub prototype that was developed by the Infocepts team. 

## How do I get set up?

### Summary of set up

The application can be hosted locally with a standard web server. The followings are the details of the software that are used.

Purpose  | Software | Download URL
------------- | ------------- | ------------- 
Desktop Web Server  | Fenix 2.0 | http://fenixwebserver.com/
Text Editor  | Brackets | http://brackets.io/

#### Dependencies ####
The application requires to be connected to DFS network as well as the Internet.

#### Deployment instructions ####
Localhost Deployment
1. Clone the repository into a local directory.
2. Launch Fenix 2.0 and create a new Web Server (Web Server > New)
3. Enter the name of the new server and select the directory of the local copy of the repository.
4. By default, the Port Number is set as 80 (may decide to change to any other port number), and click on 'Create'. 
5. The newly added server will be included in the main view of Fenix 2.0. 
6. Click on the play button on the top right corner of the server record to start the server. 
7. Click on the server record to launch the server on the browser.

#### Configuration instructions ####
For any change of service directory, please modify the directory in 'config.json' file. The application retrieves the directory of the service from 'config.json'. The file 'config.json' can be found in '../js/config.json'. The current directories included in 'config.json' are: slshdr, slsline, product, metadata.